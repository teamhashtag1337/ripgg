#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(SOCKET comSocket, int messageCode)
{
	_sock = comSocket;
	_messageCode = messageCode;
}

RecievedMessage::RecievedMessage(SOCKET comSocket, int messageCode, vector<string> values)
{
	_sock = comSocket;
	_messageCode = messageCode;
	_values = values;
}

SOCKET RecievedMessage::getSocket()
{
	return _sock;
}

User* RecievedMessage::getUser()
{
	return _user;
}

int RecievedMessage::getMessageCode()
{
	return _messageCode;
}

vector<string>& RecievedMessage::getValues()
{
	return _values;
}

void RecievedMessage::setUser(User* user)
{
	_user = user;
}