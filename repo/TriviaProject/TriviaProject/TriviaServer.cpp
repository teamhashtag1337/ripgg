
#include "Server.h"
#include "Helper.h"
#include <exception>
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <fstream>
#include <string>
#include <thread>

using namespace std;
mutex messages_mutex;
condition_variable cond;

TriviaServer::TriviaServer() : _currentUser(NULL)
{
	// notice that we step out to the global namespace
	// for the resolution of the function socket

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	
}

TriviaServer::~TriviaServer()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		while (!_users.empty())
		{
			delete _users.front();
			_users.pop();
		}

		while (!_messages.empty())
		{
			delete _messages.front();
			_messages.pop();
		}
		::closesocket(_serverSocket);
	}
	catch (...) {}
}

void TriviaServer::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << port << endl;

	thread updateThead(&TriviaServer::update, this);
	updateThead.detach();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		cout << "Waiting for client connection request" << endl;
		accept();
	}
}


void TriviaServer::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	cout << "Client accepted. Server and client can speak" << endl;

	// the function that handle the conversation with the client
	//Creating the threads here
	std::thread handleClientThread(&TriviaServer::clientHandler, this, client_socket);
	handleClientThread.detach();
}


void TriviaServer::clientHandler(SOCKET clientSocket)
{
	try
	{

		/*
		string s = "Welcome! What is your name (4 bytes)? ";
		send(clientSocket, s.c_str(), s.size(), 0);  // last parameter: flag. for us will be 0.

		//char m[5];
		recv(clientSocket, m, 4, 0);
		m[4] = 0;
		cout << "Client name is: " << m << endl;

		s = "Bye";
		send(clientSocket, s.c_str(), s.size(), 0);
		*/
		// Closing the socket (in the level of the TCP protocol)



		cond.notify_all();
		//waitToRead(*message);

		closesocket(clientSocket);
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
	}





}


void TriviaServer::update()
{
	while (true)
	{
		unique_lock<mutex> guard(messages_mutex);

		cond.wait(guard, [&](){return !_messages.empty(); });

		

	}
}

/*
void Server::waitToRead(Message& message)
{
	mutex mut;
	unique_lock<mutex> guard(mut);
	message.getCond()->wait(guard, [&](){return message.isRead(); });

}
*/


