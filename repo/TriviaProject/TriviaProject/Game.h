#pragma once
<<<<<<< HEAD
=======

>>>>>>> Gelah
#include "stdafx.h"
#include <vector>
#include <map>
#include "Question.h"
#include "User.h"

using namespace std;

class Game
{
private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questionsNo;
	int _currQuestionIndex;
	// DataBase& _db;
	map<string, int> _results;
	int _currentTurnAnswers;

	bool insertGameToDb();
	void initQuestionsFromDb();
	void sendQuestionToAllUsers();
public:
	Game(const vector<User*>& questions, int questionsNo); // add DB
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* user);
	int getId();
};