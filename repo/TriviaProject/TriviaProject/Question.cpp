#include "Question.h"

Question::Question(int id, string question, string answer1, string answer2, string answer3, string answer4)
{
	_id = id;
	_question = question;
	_answers[0] = answer1;
	_answers[1] = answer2;
	_answers[2] = answer3;
	_answers[3] = answer4;
}

string Question::getQuestion()
{
	return _question;
}

string* Question::getAnswers()
{
	return _answers;
}

int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}

int Question::getId()
{
	return _id;
}