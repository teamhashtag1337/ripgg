#pragma once

#include "stdafx.h"

class Question
{
private:
	string _question;
	string _answers[4];
	int _correctAnswerIndex;
	int _id;

public:
	Question(int id, string question, string answer1, string answer2, string answer3, string answer4); //Answer 1 is the right answer
	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();

};