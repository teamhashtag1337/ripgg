#pragma once

#include "stdafx.h"
#include <vector>
#include "User.h"

class User;

class Room
{
private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionsNo;
	string _name;
	int _id;


	string getUsersAsString(vector<User*> users, User* user);
	void sendMessage(string message);
	void sendMessage(User* user, string message);
public:
	Room(int id, User* admin, string name, int maxUsers, int questionTime, int questionsNo);
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	string getName();


};