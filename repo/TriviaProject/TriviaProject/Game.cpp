#include "Game.h"

Game::Game(const vector<User*>& players, int questionsNo)
{
	_players = players;
	_questionsNo = questionsNo;
}

Game::~Game()
{

}

bool Game::insertGameToDb()
{

}

void Game::initQuestionsFromDb()
{

}

void Game::sendFirstQuestion()
{

}

void Game::handleFinishGame()
{

}

bool Game::handleNextTurn()
{

}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{

}

bool Game::leaveGame(User* user)
{

}

int Game::getId()
{
	
}