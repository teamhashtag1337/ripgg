#pragma once

#include "stdafx.h"
#include <queue>
#include "User.h"
#include "Message.h"
#include <WinSock2.h>
#include <Windows.h>

class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:
	void accept();
	void clientHandler(SOCKET clientSocket);
	void updateClientFile(User* user);
	void update(); //Updates the messages queue
	void switchUser(); //Pops from the queue and switches the current user.
	void updateServerFile(SOCKET clientSocket); //Updates the server file with the information from the writer.
	void advanceAll(); //Advances the positions for all clients
	void updateAll(); //Sends an update message to all clients
	void disconnectUser(User* user); //Disconnects a certain user from the server
	void waitToRead(Message& message); //Waits for the message entered to be completely read
	SOCKET _serverSocket;
	std::queue<User*> _users;
	User* _currentUser;
	std::queue<Message*> _messages;
};

