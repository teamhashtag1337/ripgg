#pragma once

#include "stdafx.h"
#include "Helper.h"
#include "Room.h"
#include "Game.h"

class Room;
class Game;

class User 
{
private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;


public:
	User(string name, SOCKET sock);
	void send(string);
	string getUserName();
	SOCKET getSocket();


};