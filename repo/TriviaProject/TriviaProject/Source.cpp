#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "TriviaServer.h"
#include <iostream>
#include <exception>

using namespace std;

#define LISTENPORT 8876

int main()
{
	try
	{
		WSAInitializer wsaInit;
		TriviaServer myServer;

		myServer.serve(LISTENPORT);
	}
	catch (exception& e)
	{
		cout << "Error occured: " << e.what() << endl;
	}
	system("PAUSE");
	return 0;
}
